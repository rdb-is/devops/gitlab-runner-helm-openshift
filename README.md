This deploys a kubernetes executor for Gitlab Runner on Openshift.

# Secrets

You'll need to push a secret named `docker` with your Docker hub credentials:

```shell
oc create secret docker-registry docker \
  --docker-server=docker.io \
  --docker-username=<username> \
  --docker-password=<password> \
  --docker-email=<email>
```

and also set the registration token for the runner in `values.yml`
